(function(window) {

    'use strict';

    var a = 57;
    var b = 34;

    console.log(a);
    console.log(b);

    a = a + b;
    b = a - b;
    a = a - b;

    console.log(a);
    console.log(b);

})(window);
