(function(window) {

    'use strict';

    window.x = Object.create({});
    x.a = 'a';
    x.b = 'b';

    window.y = Object.create(x);
    y.c = 'c';
    y.d = 'd';

    window.z = Object.create(y);
    z.e = 'e';
    z.f = 'f';

    Object.prototype.getPropertyDescriptor = function(objectName, propertyName) {
        if (!arguments || arguments.length <= 1)
            throw new Error("insufficient arguments");

        var propertyDescriptor = null;

        while (objectName !== null && !propertyDescriptor) {
            if (Object.prototype.hasOwnProperty.call(objectName, propertyName))
                propertyDescriptor = Object.getOwnPropertyDescriptor(objectName, propertyName);
            else
                objectName = Object.getPrototypeOf(objectName);
        }

        if (propertyDescriptor)
            console.log(propertyDescriptor);
        else
            console.log('property not found');
    };

})(window);
